#!/usr/bin/env python

# a stacked bar plot with errorbars

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from pylab import *


fig = figure(figsize=(5,2.9), dpi=100)
ax1 = subplot(1,1,1)
matplotlib.rcParams.update({'font.size': 14})

subplots_adjust(bottom=0.2)
subplots_adjust(left=0.18)
subplots_adjust(right=0.85)

x = (1,2,3,4,5,6,7,8,9,10)
#random = (0.063,0.126,0.157,0.184,0.219,0.240,0.253,0.287,0.298,0.320)
#single = (0.123,0.251,0.429,0.571,0.683,0.768,0.806,0.840,0.898,0.928)
random = (0.753,
0.763,
0.758,
0.758,
0.765,
0.768,
0.761,
0.764,
0.771,
0.77
)
single = (0.766,
0.778,
0.771,
0.771,
0.778,
0.78,
0.775,
0.777,
0.785,
0.784
)
h1 = (1,
0.6763134908,
0.4697521962,
0.4346465726,
0.3827895776,
0.2715772991,
0.1904351052,
0.1357141536,
0.08760143383,
0
)
h2 = (0.5,
0.92,
0.06,
0.22,
0.67,
0.96,
0.03,
0.55,
0.34,
0.44
)

plot (x, random, marker='o', markersize=6, linestyle='-', color='k', label='micro-F1',markevery=1, fillstyle='none')
plot (x, single, marker='^', markersize=6, fillstyle='full', linestyle='--', color='k', label='macro-F1',markevery=1)

#plot(x,h1, c='b', marker="^",ls='--',label='GNE',fillstyle='none')
#plot(x,h2, c='g',marker=(8,2,0),ls='--',label='MMR')

#plot(x,h2, c='k', marker="o",ls='--',label='GMC')
#plot(x,x**2-1, c='m', marker="o",ls='--',label='BSwap',fillstyle='none')
#plot(x,x-1, c='k', marker="+",ls=':',label='MSD')

#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

#rc('text', usetex=True)

xlabel('Percentage of anchors', fontsize=14)
ylabel('F1 score', fontsize=14)
#fig.gca().set_ylabel('Normalized Information Loss')

xticks(np.arange(1, 11, 1), fontsize=14);
xlim(0, 11);
yticks(np.arange(0.7, 0.8, 0.02), fontsize=14);
ylim(0.7, 0.8)
plt.legend(prop={'size':12}, frameon=False, labelspacing=0,
            loc='lower left')

fig.gca().set_title('Cora dataset')
ax2 = ax1.twinx()
ax2.plot(x,h1, c='k', ls='-',label='PIP')

# xticks(np.arange(1, 11, 1));
# xlim(0, 11);
yticks(np.arange(0, 1.2, 0.2));
ylim(-0.1, 1.1)
ylabel('PIP')

plt.legend(prop={'size':12}, frameon=False, labelspacing=0,
            loc='lower left', bbox_to_anchor=(0, 0.16))



# Save the figure

savefig('../5c06955f1427192954d39f9e/figure/anchors-cora.pdf', bbox_inches='tight',dpi=300)

show()
