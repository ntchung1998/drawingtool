import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from pylab import *
import json
import os


def get_info_special_text(file_path):
    text = open(file_path, 'r').read()
    accs = re.findall(r"Accuracy: .+?[\"\n]", text)
    accs = list(map(lambda x: x.replace("Accuracy: ", "").strip(), accs))

    def to_float(x): return np.array(list(map(float, x)))
    accs = to_float(accs)
    return accs


def get_infor_from_normal_text(file_path):
    text = open(file_path, 'r').readlines()
    full_data = []
    for line in text:
        data = line.split()
        for ele in data:
            full_data.append(float(ele))
    return np.array(full_data)

def barchart(data, cluster_names, xtitle, ytitle, filename, models, yticks=None, add_Legend = False):
    """
    :param cluster_names: list of cluster names
    :param data: np array of shape (n, len(cluster_names)) where n is the number of bars in one cluster
    :param xtitle: title of x axis
    :param ytitle: title of y axis
    :return:
    """
    n_bars_in_cluster = data.shape[0]
    fig = plt.figure(dpi=100)
    ax = fig.add_subplot(111)
    index = np.arange(len(cluster_names))
    bar_width = 0.1
    opacity = 0.8

    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width, box.height*0.9])


    colors = ['#003300', '#009933', '#33cc33', '#66ff66', '#99ff99', '#ffffff']
    # print(n_bars_in_cluster)
    # raise Exception

    for i in range(n_bars_in_cluster):
        ax.bar(index + bar_width * i, data[i], bar_width, alpha=opacity, label=models[i], color=colors[i], edgecolor='#000000')

    plt.xlabel(xtitle)
    plt.ylabel(ytitle)
    plt.xticks(index + bar_width * 3, cluster_names)
    ax.set_ylim(0, 1.)
    if yticks is not None:
        plt.yticks(*yticks)

    if add_Legend:
        handles, labels = ax.get_legend_handles_labels()
        lgd = ax.legend(handles, labels, bbox_to_anchor=(1,1))
        fig.savefig(filename, bbox_extra_artists=(lgd,), bbox_inches='tight')
    else:
        fig.savefig(filename, bbox_inches='tight')
    plt.close()



def line_chart_old(data, xpoints, xtitle, ytitle, filename, models, yticks=None, add_Legend=False):
    """

    :param data: np array shape (nmodels, n_xpoints)
    :param xpoints:
    :param xtitle:
    :param ytitle:
    :return:
    """

    styles = ['o-', '>--', 's-', 'd-.', '^:', 'x-', '*-', 'v-']

    fig = plt.figure(dpi=150)
    ax = fig.add_subplot(111)
    for i in range(len(models)):
        ax.plot(np.arange(len(xpoints)), data[i], styles[i], label=models[i], markersize=12, fillstyle=None, mfc='none')
    # plot(x,h1, , marker="^",ls='--',label='GNE',fillstyle='none')

    plt.xticks(np.arange(len(xpoints)), xpoints)
    plt.xlabel(xtitle)
    plt.ylabel(ytitle)
    ax.set_ylim(0, 1.)

    if add_Legend:
        handles, labels = ax.get_legend_handles_labels()
        lgd = ax.legend(handles, labels, bbox_to_anchor=(1.,1))
        fig.savefig(filename, bbox_extra_artists=(lgd,), bbox_inches='tight')
    else:
        print(filename)
        fig.savefig(filename, bbox_inches='tight')
    plt.close()



def line_chart(models, data_matrix, x_label, y_label, title, xpoints, highermodels = [], name=None, maxx=1.2):
    # styles = ['o', '>', 's', 'd', '^', 'x', '*', 'v']
    styles = ['o', 's', 'v', '*']
    line_styles = ['-', '--', '-', '-.', ':']
    # styles = ['o-', '>--', 's-', 'd-.', '^:', 'x-', '*-', 'v-']
    
    colors = ['#003300', '#009933', '#33cc33', '#66ff66', '#99ff99', '#ffffff']
    barwith = 0.1
    ax1 = plt.subplot(111)

    nummodels = data_matrix.shape[0]
    num_x_levels = data_matrix.shape[1]\
        
    assert nummodels == len(models), "Number of model must equal to data matrix shape 0"


    lns1 = []
    for i, model in enumerate(models):
        if model not in highermodels:
            line = data_matrix[i]
            x = np.arange(num_x_levels)
            fillstyle = 'none'
            if i > 3:
                fillstyle = 'full'
            lni, = ax1.plot(x, line, marker=styles[i % len(styles)], markersize=8, color='k', label=models[i], markevery=1, fillstyle=fillstyle)
            lns1.append(lni)

    ax1.set_xlabel(x_label)
    ax1.set_ylabel(y_label)
    if title is not None:
        plt.title(title, loc='center', color='black', fontsize=28)

    ax2 = None
    lns2 = []
    count = 0

    for i, model in enumerate(models):
        if model in highermodels:
            line = data_matrix[i]
            x = np.arange(num_x_levels)
            if ax2 is None:
                ax2 = ax1.twinx()
            ln2 = ax2.bar(x + count * barwith, line, width=barwith, color = colors[count],  edgecolor='k', label=model, alpha = 0.5)
            lns2.append(ln2)
            count += 1


    plt.xticks(np.arange(len(xpoints)), xpoints)
    ax1.set_xlim(-0.3, len(xpoints) + .3 - 1)
    ax1.set_ylim(-0.05, maxx + .22)
    ax1.set_yticks(np.arange(0, 1.1, 0.2))

    if ax2 is not None:
        ax2.set_xlim(-0.5, len(xpoints) + .5 - 1)
        ax2.set_ylim(0, 0.7)
        ax2.set_yticks(np.arange(0, 0.6, 0.1))
        ax2.tick_params('y', colors='green')

    ax1.grid(True)

    plt.legend(ncol = 3, loc=9, fontsize=13)
    plt.savefig(name)
    plt.close()




if __name__ == "__main__":
    # old data
    supervisedmodels = ['NAWAL', 'PALE', 'DeepLink']
    unsupervisedmodels = ['NAWAL', 'REGAL', 'FINAL', 'BigAlign']

    modes = ['sup', '']
    datas = ['ppi', 'bn', 'econ']
    # xpoint_del_nodes = {"xp": ["0", "0.01", "0.05", "0.1", "0.2"], "xlabel": "Nodes removal ratio"}
    xpoint_del_edges = {"xp": ["0", "0.01", "0.05", "0.1", "0.2"], "xlabel": "Edges removal ratio"}

    for mode in modes:
        for data in datas:
            file_name = 'data/del_edges' + '_' + data
            if mode == 'sup':
                file_name += '_' + 'sup'
            accs = get_infor_from_normal_text(file_name)
            if mode == 'sup':
                models = supervisedmodels
            else:
                models = unsupervisedmodels
            accs = accs.reshape((len(models), -1))
            xticks = xpoint_del_edges['xp']
            xlabel = xpoint_del_edges['xlabel']
            if mode == 'sup':
                barchart(data=accs, cluster_names=xticks, xtitle=xlabel, ytitle="Accuracy", filename='{}_sup.png'.format(data), models=models, yticks=None, add_Legend=True)
            else:
                line_chart_old(data=accs, xpoints=xticks, xtitle=xlabel, ytitle="Accuracy", filename='{}.png'.format(data), models=models, yticks=None, add_Legend=True)



    models = ['NAWAL', 'PALE', 'DeepLink', 'FINAL', 'REGAL']
    modes = ['del_edges', 'del_nodes']
    datasets = ['facebook', 'foursquare', 'twitter']
    xpoint_del_nodes = {"xp": ["0", "0.01", "0.05", "0.1", "0.2", "0.3", "0.4"], "xlabel": "Nodes removal ratio"}
    xpoint_del_edges = {"xp": ["0", "0.01", "0.05", "0.1", "0.2", "0.3", "0.4"], "xlabel": "Edges removal ratio"}
    xpoints = {'del_edges': xpoint_del_edges, 'del_nodes': xpoint_del_nodes}
    ylabel = "Accuracy"
    maxx = 1.05
    for dataset in datasets:
        for mode in modes:
            file_name = "data/{}_{}.txt".format(mode, dataset)
            name = mode + '_' + dataset
            accs = get_info_special_text(file_name)
            accs = accs.reshape((len(models) + 1, -1))
            accs = np.delete(accs, 1, axis=0)
            xticks = xpoints[mode]["xp"]
            xlabel = xpoints[mode]["xlabel"]
            line_chart(models = models, data_matrix=accs, \
                x_label=xlabel, y_label=ylabel, title=None, xpoints=xticks, highermodels=[], name='{}_black.png'.format(name), maxx=maxx)
            
            if mode == 'del_nodes':
                line_chart_old(data=accs, xpoints=xticks, xtitle=xlabel, ytitle=ylabel, filename='{}.png'.format(name), models=models, yticks=None, add_Legend=True)
            if mode == 'del_edges':
                barchart(data=accs, cluster_names=xticks, xtitle=xlabel, ytitle=ylabel, filename='{}.png'.format(name), models=models, yticks=None, add_Legend = True)
