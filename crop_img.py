import cv2
import os


files = os.listdir('.')
# print(files)
image_files = [ele for ele in files if ele != 'Legends.png' and ele.endswith('png')]
# print(image_files)

for f in image_files:
    img = cv2.imread(f)
    # print(img.shape())
    crop_img = img[97:1500, 45:1365]
    # cv2.imshow("crop_img", crop_img)
    cv2.imwrite(f, crop_img)
    # cv2.waitKey(0)
    # print(crop_img.shape)

"""
img = cv2.imread("lenna.png")
crop_img = img[y:y+h, x:x+w]
cv2.imshow("cropped", crop_img)
cv2.waitKey(0)
"""