import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from pylab import *
import json
import os

# matplotlib.rcParams.update({'font.size': 30})
# matplotlib.rcParams['text.usetex'] = True

def get_info_from_seed_file(file_path, key_str, last):
    keys_to_search_ele = [key_str + last[i] for i in range(len(last))]
    
    data = json.load(open(file_path))
    keys = list(data.keys())
    keys_to_search = []
    for k in keys_to_search_ele:
        for key_data in keys:
            if key_data.endswith(k):
                keys_to_search.append(key_data)
                break
    data_for_model = []
    for key in keys_to_search:
        value = 0
        count = 0
        data_key = data[key]
        seeds = list(data_key.keys())
        for seed in seeds:
            data_key_seed = data_key[seed]
            if len(data_key_seed) > 0:
                try:
                    acc = data_key_seed['acc']
                    value += acc
                    count += 1
                except:
                    continue
        if count > 0:
            value /= count
        data_for_model.append(value)
    return data_for_model

def get_info_special_text(file_path):
    text = open(file_path, 'r').read()
    accs = re.findall(r"Accuracy: .+?[\"\n]", text)
    maps = re.findall(r"MAP: .+?[\"\n]", text)
    aucs = re.findall(r"AUC: .+?[\"\n]", text)
    hits = re.findall(r"Hit-precision: .+?[\"\n]", text)
    top5s = re.findall(r"Precision_5: .+?[\"\n]", text)
    top10s = re.findall(r"Precision_10: .+?[\"\n]", text)
    times = re.findall(r"Time: .+?(?=[\"\n\t])", text)

    accs = list(map(lambda x: x.replace("Accuracy: ", "").strip(), accs))
    maps = list(map(lambda x: x.replace("MAP: ", "").strip(), maps))
    aucs = list(map(lambda x: x.replace("AUC: ", "").strip(), aucs))
    hits = list(map(lambda x: x.replace("Hit-precision: ", "").strip(), hits))
    top5s = list(map(lambda x: x.replace("Precision_5: ", "").strip(), top5s))
    top10s = list(map(lambda x: x.replace("Precision_10: ", "").strip(), top10s))
    times = list(map(lambda x: x.replace("Time: ", "").replace("Time used for alignment: ","").strip(), times))

    def to_float(x): return np.array(list(map(float, x)))
    accs = to_float(accs)
    maps = to_float(maps)
    aucs=to_float(aucs)
    hits=to_float(hits)
    top5s=to_float(top5s)
    top10s=to_float(top10s)
    times=to_float(times)

    return (accs,
        maps,
        aucs,
        hits,
        top5s,
        top10s,
        times)
    # return accs




def line_chart(models, data_matrix, x_label, y_label, title, xpoints, higher_models = [], name=None, maxx=1.2):

    maxx = maxx - 0.2
    # styles = ['o', '>', 's', 'd', '^', 'x', '*', 'v']
    styles = ['o', 'v', 's', '*']
    line_styles = ['-', '--', '-', '-.', ':']
    # styles = ['o-', '>--', 's-', 'd-.', '^:', 'x-', '*-', 'v-']
    
    colors = ['#003300', '#009933', '#33cc33', '#66ff66', '#99ff99', '#ffffff']
    barwith = 0.1
    # ax1 = plt.subplot(111)
    fig, ax1 = plt.subplots(figsize=(10, 9))

    num_models = data_matrix.shape[0]
    num_x_levels = data_matrix.shape[1]
        
    assert num_models == len(models), "Number of model must equal to data matrix shape 0"


    lns1 = []
    for i, model in enumerate(models):
        if model not in higher_models:
            marker = styles[i % len(styles)]
            if model == 'BigAlign':
                marker = '*'
            if model == 'IsoRank':
                marker = 's'
            line = data_matrix[i]
            x = np.arange(num_x_levels)
            fillstyle = 'none'
            if i > 3:
                fillstyle = 'full'
            lni, = ax1.plot(x, line, marker=marker, markersize=25, color='k', label=models[i], markevery=1, fillstyle=fillstyle)
            lns1.append(lni)

    # ln11, = ax1.plot(x, random, marker='s', markersize=8, linestyle='-', color='k', label='micro-F1', markevery=1, fillstyle='full')
    # ln12, = ax1.plot(x, single, marker='^', markersize=8, linestyle='--', color='k', label='macro-F1', markevery=1, fillstyle='full')

    ax1.set_xlabel(x_label, labelpad = 10, fontsize=45)
    # ax1.set_ylabel(y_label, fontsize=25)
    title = "Accuracy"
    if title is not None:
        plt.title(title, loc='center', color='black', fontsize=45)

    ax2 = None
    lns2 = []
    count = 0

    for i, model in enumerate(models):
        if model in higher_models:
            line = data_matrix[i]
            x = np.arange(num_x_levels)
            if ax2 is None:
                ax2 = ax1.twinx()
            # ln2 = ax2.bar(x + count * barwith, line, width=barwith, color = 'green', align='center',  edgecolor='green', label=model, alpha = 0.5)
            ln2 = ax2.bar(x + count * barwith, line, width=barwith, color = colors[count],  edgecolor='k', label=model, alpha = 0.5)
            lns2.append(ln2)
            count += 1

        # plt.title('PIP', loc='right', color='green', fontsize=28)

    # xpoints = ["0", "0.01", "0.05", "0.1", "0.2"]
    # plt.xticks(np.arange(len(xpoints)), xpoints, fontsize=10)
    ax1.tick_params(labelsize=35)
    ax1.set_xticks(np.arange(len(xpoints)))
    ax1.set_xticklabels(xpoints)
    ax1.set_xlim(-0.3, len(xpoints) + .3 - 1)
    ax1.set_ylim(-0.05, maxx + .05)
    print(maxx)
    ax1.set_yticks(np.arange(0, maxx + .1, 0.2))

    if ax2 is not None:
        ax2.set_xlim(-0.5, len(xpoints) + .5 - 1)
        ax2.set_ylim(0, 0.7)
        ax2.set_yticks(np.arange(0, 0.6, 0.1))
        ax2.tick_params('y', colors='green')

    # ax1.yaxis.grid(True)
    ax1.grid(True)


    # plt.legend((ln11, ln12, ln2), ('micro-F1','macro-F1','PIP'), fontsize=18, loc='upper right', frameon=False, 
    #            columnspacing=0.8, labelspacing=0, ncol=3, borderpad=0, handletextpad=0.2)

    # plt.legend((ln11, ln12), ('micro-F1','macro-F1'), fontsize=18, loc='upper right', frameon=False, 
    #            columnspacing=0.8, labelspacing=0, ncol=3, borderpad=0, handletextpad=0.2)
    # plt.legend(ncol = 3, loc=9, fontsize=13)
    if len(models) == 3:
        plt.legend(ncol = 3, loc=9, fontsize=25)


    # dir = "./"
    # # savefig(dir + "anchors-bc.pdf", bbox_inches='tight', dpi=300)
    plt.tight_layout()
    plt.savefig(name)
    plt.close()


if __name__ == "__main__":
    # models = ['NAWAL', 'PALE', 'DeepLink', 'FINAL', 'REGAL']
    # modes = ['del_edges', 'del_nodes']
    # datasets = ['facebook', 'foursquare', 'twitter']
    # xpoint_del_nodes = {"xp": ["0", "0.01", "0.05", "0.1", "0.2", "0.3", "0.4"], "xlabel": "Nodes removal ratio"}
    # xpoint_del_edges = {"xp": ["0", "0.01", "0.05", "0.1", "0.2", "0.3", "0.4"], "xlabel": "Edges removal ratio"}
    # xpoints = {'del_edges': xpoint_del_edges, 'del_nodes': xpoint_del_nodes}
    # ylabel = "Accuracy"
    # maxx = 1.05
    # for dataset in datasets:
    #     for mode in modes:
    #         file_name = "data/{}_{}.txt".format(mode, dataset)
    #         name = mode + '_' + dataset
    #         accs = get_info_special_text(file_name)
    #         accs = accs.reshape((len(models) + 1, -1))
    #         accs = np.delete(accs, 1, axis=0)
    #         xticks = xpoints[mode]["xp"]
    #         xlabel = xpoints[mode]["xlabel"]
    #         line_chart(models = models, data_matrix=accs, \
    #             x_label=xlabel, y_label=ylabel, title=None, xpoints=xticks, higher_models=[], name='{}_black.png'.format(name), maxx=maxx)
    
            


    # exit()
    models = ["REGAL","FINAL","IsoRank","BigAlign","PALE","IONE","DeepLink"]
    data_matrix_connectivity = []
    data_matrix_components = []
    
    for model in models:
        path = 'data/' +  model + '/connectivity/seeds.json'
        key_str = 'fully-synthetic-small-world-n1000-k10-p'
        xpoints_connectivity = ["0.2", "0.3", "0.4", "0.5", "0.6", "0.7"]
        last = [ele[-1] for ele in xpoints_connectivity]
        data_model = get_info_from_seed_file(path, key_str, last)
        data_matrix_connectivity.append(data_model)

        path = 'data/' + model + '/components/seeds.json'
        xpoints_component = ["1", "2", "3", "4", "5", "6"]
        last = [ele[-1] for ele in xpoints_component]
        key_str = 'fully-synthetic-connected-components-small-world-n1000-k10-p5-nc'
        data_model = get_info_from_seed_file(path, key_str, last)
        data_matrix_components.append(data_model)


    data_matrix_connectivity = np.array(data_matrix_connectivity)
    data_matrix_components = np.array(data_matrix_components)

    # higher_models = ["FINAL", "PALE", "IONE"]
    # higher_models = ["REGAL", "IsoRank", "BigAlign", "DeepLink"]
    higher_models = []

    # density
    path_density = 'data/density.txt'
    xpoints_density = ["2", "60", "100", "200", "700", "999"]
    data_matrix_density = get_info_special_text(path_density)[0].reshape((len(models), len(xpoints_density)))
    xlabel_density = 'k'
    title_density = None


    
    ylabel = "Accuracy"
    line_chart(models = models, data_matrix=data_matrix_density, \
        x_label=xlabel_density, y_label=ylabel, title=title_density, xpoints=xpoints_density, higher_models=higher_models, name='density_black.png')
    # connectivity
    xpoints_connectivity = ["0.2", "0.3", "0.4", "0.5", "0.6", "0.7"]
    titile_connectivity = None
    xlabel_connectivity = 'p'


    line_chart(models = models, data_matrix=data_matrix_connectivity, \
        x_label=xlabel_connectivity, y_label=ylabel, title=titile_connectivity, xpoints=xpoints_connectivity, higher_models=higher_models, name='connectivity_black.png')
    

    xpoints_component = ["1", "2", "3", "4", "5", "6"]
    title_component = None
    xlabel_component = "Connected components"


    line_chart(models = models, data_matrix=data_matrix_components, \
        x_label=xlabel_component, y_label=ylabel, title=title_component, xpoints=xpoints_component, higher_models=higher_models, name='components_black.png')


    datasets = ['econ', 'bn', 'ppi']
    modes = ['change_feats', 'del-edges', 'del-nodes']
    key_strs = {'change_feats': 'REGAL-d05-pfeats', 'del-edges': 'REGAL-d', 'del-nodes': 'del-nodes-p'}
    zero_noises = {"REGAL": {"econ": 0.9746, "bn": 0.9719, "ppi": 1.0000},
                   "REGAL2": {"econ": 0.6598, "bn": 0.7491, "ppi":  0.7650},
                   "FINAL": {"econ": 0.3490, "bn": 0.4868, "ppi":  0.5470},
                   "FINAL2": {"econ": 0.3211, "bn": 0.4835, "ppi":  0.5338},
                   "IsoRank": {"econ": 1.0000, "bn": 0.9259, "ppi": 0.9967},
                   "BigAlign": {"econ": 0.5811, "bn": 0.5918, "ppi": 0.8521},
                   "BigAlign2": {"econ": 0.5286, "bn": 0.3586, "ppi": 0.5250},
                   "PALE": {"econ": 0.9940, "bn": 0.9207, "ppi": 0.9979},
                   "IONE": {"econ": 0.6236, "bn": 0.4954, "ppi":  0.8599},
                   "DeepLink": {"econ": 0.7488, "bn": 0.4456, "ppi": 0.6071}}
    xpoint_change_feats = {"xp": ["0", "0.1", "0.2", "0.3", "0.4", "0.5"], "xlabel": "Features changing ratio"}
    xpoint_del_nodes = {"xp": ["0", "0.1", "0.2", "0.3", "0.4", "0.5"], "xlabel": "Nodes removal ratio"}
    xpoint_del_edges = {"xp": ["0", "0.01", "0.05", "0.1", "0.2"], "xlabel": "Edges removal ratio"}
    xpoints = {'change_feats': xpoint_change_feats, 'del-edges': xpoint_del_edges, 'del-nodes': xpoint_del_nodes}
    for dataset in datasets:
        for mode in modes:
            name = mode + '-' + dataset
            data_matrix = []
            key_str = key_strs[mode]
            xticks = xpoints[mode]["xp"]
            xlabel = xpoints[mode]["xlabel"]
            last = [ele.split('.')[-1] for ele in xticks]
            maxx = 1.2 
            if mode == 'change_feats':
                maxx = 1.1
                models = ['REGAL', 'FINAL', 'BigAlign']
            else:
                models = ["REGAL","FINAL","IsoRank","BigAlign","PALE","IONE","DeepLink"]
            for model in models:
                path = 'data/{}/{}/seeds.json'.format(model, name)
                if mode == "change_feats":
                    m = model + '2'
                else:
                    m = model
                zero_value = [zero_noises[m][dataset]]
                data_model = get_info_from_seed_file(path, key_str, last)
                data_model = zero_value + data_model
                data_matrix.append(data_model)
            data_matrix = np.array(data_matrix)
            
            line_chart(models = models, data_matrix=data_matrix, \
                x_label=xlabel, y_label=ylabel, title=None, xpoints=xticks, higher_models=[], name='{}_black.png'.format(name), maxx=maxx)
    
            